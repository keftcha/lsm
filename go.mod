module gitlab.com/keftcha/lsm

go 1.21

require (
	github.com/Cloud-Foundations/Dominator v0.0.0-20200412162041-d8f7d123ad7d
	golang.org/x/sys v0.0.0-20200615200032-f1bc736245b1
)
